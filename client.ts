import * as Vorpal from 'vorpal'
const vorpal = Vorpal()
import * as got from 'got'
import * as crypto from '@shardus/crypto-utils'

// Note we need to make sure this is the same in here as in index.ts. Otherwise
// when we hash an object, and the server hashes that same object, the resulting
// hashes will be different. This string is like a salt.
crypto.init('abcd123abcd123abcdef3123456e5083934424abcfab9eee8765423111111111')

// Talking with the server consists of sending HTTP GET requests and HTTP POST
// requests. The latter sends data in the shape of a Transaction. You'll see
// the abbreviation 'tx'. We call this "injecting a transaction".
const INJECT_URL = `http://localhost:9001/inject`
const STATE_URL = `http://localhost:9001/state`

async function postJSON(url, obj): Promise<string> {
  const response = await got.post(url, {
    method: 'POST',
    headers: { 'content-type': 'application/json' },
    body: JSON.stringify(obj)
  })
  return response.body
}

vorpal
  .command('state', 'Queries the network via a GET request to /state.')
  .action(async (args, cb) => {
    const res = await got(STATE_URL)
    vorpal.log(JSON.parse(res.body))
    cb()
  })

vorpal
  .command(
    'set state <state> <account>',
    `Injects a tx into the network via a POST request to /inject. Whatever
    account name you give it, this client will hash that to create a 32 byte
    hex string suitable for the server to use as an account address.`
  )
  .action(async (args, callback) => {
    const tx = {
      state: args.state,
      accountId: crypto.hash(args.account),
      timestamp: Date.now()
    }

    const res = await postJSON(INJECT_URL, tx)
    vorpal.log(JSON.parse(res))
    callback()
  })

vorpal
  .delimiter('client$')
  .show()
