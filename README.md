# Hello Cloud
#### A trivially simple app built on top of the [shardus](https://gitlab.com/shardus) network.

## What's going on here?

This is an example app to demonstrate the `shardus` distributed network. Its aim is to be as dead simple as possible.
To that end, this app does two things:

1) Writes state to the `shardus` distributed network.
2) Reads that state from the network.

To run the app, open up two terminal tabs here in this folder.
In the first tab, run:
```sh
npm run start
```

And in the second tab:
```sh
npm run client
```

From within the client, you can run a couple different commands.

Set state into the network. Supply any number for the first argument, and any string for the second.
The string you supply as the second argument will be converted, _on the client_, to a hash appropriate
for account Ids.
```sh
set state <number> <account>
```

Read the state of the server.
```sh
state
```

This first command fires up a network of `shardus` nodes on your local machine, and
the second runs the `client.ts` script, which is just a simple way to automate
making http calls to the `shardus` network. You could just as well use `curl` or `wget`
or your favorite JSON request tool.

When you want to stop the server, ctl-c out of the first terminal, then run
```sh
npm run stop  # instruct our process manage (pm2) to stop all background processes
npm run clean # remove log folders created by the network.
```
